package com.canway.book_store;
import java.util.HashMap;

import org.junit.Test;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.RunWith;
import org.junit.runner.notification.Failure;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import com.canway.book_store.controller.BootStoreController;


@RunWith(SpringRunner.class)
@SpringBootTest
public class BookStoreApplicationTests {

    @Autowired
    private BootStoreController bookStoreController;

    @Test
    public void contextLoads() {
    }

    @Test
    public void testTestParam() {
        HashMap<String, String> result = bookStoreController.testParam("test");
        assert(result.get("message").equals("This is what you input: test"));
    }

    @Test
    public void testBuyBookC() {
        HashMap<String, String> params = new HashMap<>();
        params.put("book", "C");
        HashMap<String, String> result = bookStoreController.buyBook(params);
        assert(result.get("price").equals("35 Yuan"));
    }

    @Test
    public void testBuyBookJava() {
        HashMap<String, String> params = new HashMap<>();
        params.put("book", "Java");
        HashMap<String, String> result = bookStoreController.buyBook(params);
        assert(result.get("price").equals("35 Yuan"));
    }

    @Test
    public void testBuyBookFail() {
        HashMap<String, String> params = new HashMap<>();
        params.put("book", "Fail");
        HashMap<String, String> result = bookStoreController.buyBook(params);
        assert(result.get("price").equals("No price"));
    }

    @Test
    public void testBuyBookNoBook() {
        HashMap<String, String> params = new HashMap<>();
        params.put("NoBook", "Fail");
        HashMap<String, String> result = bookStoreController.buyBook(params);
        assert(result.get("message").equals("Please add book key as post param!"));
    }

//    public static void main(String[] args) {
//        Result result = JUnitCore.runClasses(BookStoreApplicationTests.class);
//        for (Failure failure : result.getFailures()) {
//            System.out.println(failure.toString());
//        }
//        if (result.wasSuccessful()) {
//            System.out.println("所有测试用列执行成功");
//        }
//    }

}
