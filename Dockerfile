#FROM centos
#MAINTAINER jasen
#
#RUN mkdir -p /usr/local/jasen
#
#ADD jdk /usr/local/jasen/jdk
#ADD tomcat /usr/local/jasen/tomcat
#
#ENV JAVA_HOME /usr/local/jasen/jdk
#ENV CATALINA_HOME /usr/local/jasen/tomcat
#ENV PATH $PATH:$JAVA_HOME/bin:$CATALINA_HOME/bin
#
#

FROM tomcat

WORKDIR /usr/local/tomcat/webapps/

ADD ./target/book.war .

EXPOSE 8080

CMD ["/usr/local/tomcat/bin/catalina.sh", "run"]

